# Testík pro PHP programátora #

Následující zadání testíku by vám nemělo zabrat více než 3 hodiny času, ale lze aplikaci napsat i za 1 hodinu, záleží na tom, jak k aplikaci přistoupíte. Aplikace nebude nikdy reálně naszaena, jde nám hlavně o váš přístup k aplikaci, její návrh a zpracování. 

### Aplikační logika ###

Aplikace bude fungovat jako jednoduchý adresář. V aplikaci bude možné vytvářet, mazat a editovat jednotlivé kontakty, které budou mít tyto položky:

* Jméno a příjmení
* Telefonní číslo
* Email
* Dlouhá poznámka

Aplikace nebude obsahovat žádné prověření ani práva, každý, kdo zná URL aplikace bude mít přístup ke všem funkcím aplikace.

### Prezentační vrstva ###

Pro aplikaci není připraven grafický návrh – budou použity základní HTML prvky bez CSS, pro zobrazování dat budou použity tabulky.
Aplikace bude SEO optimalizována, takže na základní adrese (/) bude seznam všech kontaktů, editace jednotlivých kontaktů bude na adrese (/identifikator-kontaktu). Identifikátor bude obsahovat jméno kontaktu. Mazání kontaktu může probíhat na editaci kontaktu nebo může být dostupné přes jiný odkaz – ten nemusí být SEO optimalizován (stačí ID).

### Administrační vrstva ###

Aplikace nebude mít žádnou admnistraci – vše bude probíhat bez nutnosti přihlášení..

### Interní poznámky ###

Při vytváření aplikace dbejte na základní pravidla programování a myslete zejména na bezpečnost, znovupoužitelnost a rozšiřitelnost. Aplikaci vytvářejte jako byste byli v týmu a bylo třeba, aby na vašem kódu později pokračovali další kolegové.
Používat lze veškeré běžně dostupné knihovny, frameworky běžně funkční na standardně nastaveném serveru PHP 5.6 (myslete i na to, že existuje PHP 7, kdy by bylo ideální aby váš kód bylo možné jednoduše upgradovat na 7). Pokud bude potřeba instalovat aplikaci (např. přes composer, je třeba to uvést do readme).
Pro zadavatele je důležité jak je aplikace napsána, nikoliv kolik funkcí má navíc, prosím tedy o minimalizaci času nad ní stráveného a sdělení kolik času bylo třeba aby byla aplikace uvedena do stavu, který budete posílat.